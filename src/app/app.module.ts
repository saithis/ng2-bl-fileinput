import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BlFileinputModule } from 'ng2-bl-fileinput';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BlFileinputModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
