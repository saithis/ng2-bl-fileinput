import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent {
    public form: FormGroup;
    public eventsStringSingle = '';
    public eventsStringMultiple = '';
    public customFilesListItems: File[];

    constructor(private fb: FormBuilder) {
        this.form = this.fb.group({
            file: [],
            files: [],
            customFilesList: [],
            required: [null, Validators.required]
        });
    }

    public onChangedSingle(event: File | File[]) {
        console.log('change single', event);
        this.eventsStringSingle += this.stringifyEvent(event) + '\r\n';
    }

    public onChangedMultiple(event: File | File[]) {
        console.log('change multiple', event);
        this.eventsStringMultiple += this.stringifyEvent(event) + '\r\n';
    }

    public onChangedCustomList(event) {
        this.customFilesListItems = event;
    }

    public removeFromCustomFilesList(file: File){
        const files = this.form.value.customFilesList.filter(x => x !== file);
        this.form.patchValue({
            customFilesList: files
        });
    }

    private stringifyEvent(event: File | File[]) {
        if (!event) {
            return event + '';
        }

        const serializeableEvent = Array.isArray(event)
            ? event.map(x => this.convertFileToObject(x))
            : this.convertFileToObject(event);

        return JSON.stringify(serializeableEvent);
    }

    private convertFileToObject(file: File) {
        return {
            lastModified: file.lastModified,
            name: file.name,
            size: file.size,
            type: file.type
        };
    }
}
