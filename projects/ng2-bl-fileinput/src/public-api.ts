/*
 * Public API Surface of ng2-bl-fileinput
 */

export * from './lib/bl-fileinput.component';
export * from './lib/bl-fileinput.module';
