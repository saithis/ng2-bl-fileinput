import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    forwardRef,
    HostBinding,
    Input,
    Output,
    ViewEncapsulation
} from '@angular/core';

@Component({
    selector: 'bl-fileinput',
    templateUrl: './bl-fileinput.component.html',
    styleUrls: ['./bl-fileinput.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BlFileinputComponent),
            multi: true
        }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class BlFileinputComponent implements ControlValueAccessor {
    @Input() buttonText = 'choose file(s)';
    @Input() buttonOnly = false;
    @Input() placeholder = '';
    @Input() icon = '';
    @Input() multiple = false;
    @Input() accept: string | string[] = null;
    @Output() fileChanged = new EventEmitter<File | File[]>();
    public files: File[] = [];
    @HostBinding('class.bl-fileinput__host--has-value') public get hostFilledCssClass() {
         return this.files.length > 0;
    }

    private emitFormChangeCallback = (_: any) => { };

    constructor() {

    }

    public getAccept(): string {
        if (!this.accept) {
            return '';
        }

        if (typeof this.accept === 'string') {
            return this.accept;
        }

        if (Array.isArray(this.accept)) {
            return this.accept.join(',');
        }

        throw new TypeError('"accept" has to be a string or an array of strings.');
    }

    public fileChange(event) {
        const oldFileCount = this.files.length;
        if (!this.multiple) {
            this.files = [];
        }

        const selectedFiles: File[] = event.target.files;
        for (const file of selectedFiles) {
            this.files.push(file);
        }

        this.emitChange(oldFileCount);
    }

    public removeFile(file) {
        const oldFileCount = this.files.length;
        this.files = this.files.filter(x => x !== file);
        this.emitChange(oldFileCount);
    }

    private emitChange(oldFileCount: number, notifyAngular = true) {
        // don't emit, if no file was added or removed in multiple mode
        if (this.multiple && oldFileCount === this.files.length) {
            return;
        }

        // if no file was selected and still no file is selected, don't emit the change event
        if (oldFileCount === 0 && this.files.length === 0) {
            return;
        }

        // TODO: maybe in the future add a check, if the selected files are the same

        const newformValue = this.multiple ? this.files : this.files[0];
        if (notifyAngular) {
            this.emitFormChangeCallback(newformValue);
        }
        this.fileChanged.emit(newformValue);
    }

    /**
     * Called, when the form gets a new value (setValues, etc.)
     */
    public writeValue(formValues: any): void {
        if (formValues && !Array.isArray(formValues)) {
            formValues = [formValues];
        }

        const oldFileCount = this.files.length;
        this.files = formValues || [];
        this.emitChange(oldFileCount, false);
    }

    /**
     * Registers handler, that should be called, when the input value changes from the view
     */
    public registerOnChange(fn: any): void {
        this.emitFormChangeCallback = fn;
    }

    /**
     * Registers handler, that should be called, when out control receives a touch event
     */
    public registerOnTouched(fn: any): void {

    }
}
