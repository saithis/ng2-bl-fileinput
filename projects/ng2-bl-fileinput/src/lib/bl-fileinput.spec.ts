import { ReactiveFormsModule } from '@angular/forms';
import { TestBed, async } from '@angular/core/testing';
import { BlFileinputComponent } from './bl-fileinput.component';

/* tslint:disable:no-unused-variable */
describe('BlFileinputComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                BlFileinputComponent
            ],
            imports: [
                ReactiveFormsModule,
            ]
        });
        TestBed.compileComponents();
    });

    it('should create the component', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    it('getAccept() should return array accept input as comma separated string', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        component.accept = ['image/*', '.txt', 'application/json'];

        expect(component.getAccept()).toBe('image/*,.txt,application/json');
    }));

    it('getAccept() should return string accept input as is', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        component.accept = 'image/*,.txt,application/json';

        expect(component.getAccept()).toBe('image/*,.txt,application/json');
    }));

    it('getAccept() should return empty string, if accept input is not set', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        component.accept = undefined;

        expect(component.getAccept()).toBe('');
    }));

    it('getAccept() throw, if accept input is neither array, string nor unset', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        component.accept = <any>1;

        expect(component.getAccept).toThrow();
    }));


    it('don\'t emit change event, when it changed from nothing to nothing through writeValue', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        spyOn(component.fileChanged, 'emit');

        component.writeValue(undefined);

        expect(component.fileChanged.emit).toHaveBeenCalledTimes(0);
    }));

    it('emit change event with the file, when it changed from nothing to a file through writeValue', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        spyOn(component.fileChanged, 'emit');

        const file = new File(['some file content'], 'test.txt');
        component.writeValue(file);

        expect(component.fileChanged.emit).toHaveBeenCalledWith(file);
    }));

    it('emit change event with the new file, when it changed from some old file to a new file through writeValue', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;
        component.files = [new File(['file that will be overwritten'], 'old.txt')];

        spyOn(component.fileChanged, 'emit');

        const file = new File(['some file content'], 'test.txt');
        component.writeValue(file);

        expect(component.fileChanged.emit).toHaveBeenCalledWith(file);
    }));

    it('emit change event with the new files, when the files changed through writeValue', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;
        component.files = [new File(['content'], 'old.txt')];
        component.multiple = true;

        spyOn(component.fileChanged, 'emit');

        const newFiles = [
            new File(['some file content'], 'test.txt'),
            new File(['some other file content'], 'test2.txt'),
        ];
        component.writeValue(newFiles);

        expect(component.fileChanged.emit).toHaveBeenCalledWith(newFiles);
    }));


    it('emit change events, when the user selects a file', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        const newFiles = [
            new File(['some file content'], 'test.txt'),
        ];
        component.fileChange({ target: { files: newFiles } });

        expect(component.fileChanged.emit).toHaveBeenCalledWith(newFiles[0]);
        expect(angularNotified).toBeTruthy();
    }));

    it('emit change events, when the user selects a new file', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;
        component.files = [new File(['content'], 'old.txt')];

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        const newFiles = [
            new File(['some file content'], 'test.txt'),
        ];
        component.fileChange({ target: { files: newFiles } });

        expect(component.fileChanged.emit).toHaveBeenCalledWith(newFiles[0]);
        expect(angularNotified).toBeTruthy();
    }));

    it('clear selected files and emit change events, when the user cancels the file dialog', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;
        component.files = [new File(['content'], 'old.txt')];

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        component.fileChange({ target: { files: [] } });

        expect(component.fileChanged.emit).toHaveBeenCalledWith(undefined);
        expect(angularNotified).toBeTruthy();
    }));

    it('emit change events with all files, when the user selects new files in multiple mode', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        const oldFiles = [
            new File(['old file content'], 'oldFile.txt')
        ];
        component.files = [...oldFiles];
        component.multiple = true;

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        const newFiles = [
            new File(['some file content'], 'test.txt'),
            new File(['some other file content'], 'test2.txt'),
        ];
        component.fileChange({ target: { files: newFiles } });

        const expectedFiles = [...oldFiles, ...newFiles];
        expect(component.fileChanged.emit).toHaveBeenCalledWith(expectedFiles);
        expect(angularNotified).toBeTruthy();
    }));

    it('emit change events with all remaining files, when the user removes a file', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;

        const filesThatRemain = [
            new File(['content'], 'file.txt'),
        ];
        const fileToRemove = new File(['content'], 'file_to_remove.txt');
        component.files = [...filesThatRemain, fileToRemove];
        component.multiple = true;

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        component.removeFile(fileToRemove);

        expect(component.fileChanged.emit).toHaveBeenCalledWith(filesThatRemain);
        expect(angularNotified).toBeTruthy();
    }));

    it('don\'t emit change events, when the user cancels the dialog in multiple mode', async(() => {
        const fixture = TestBed.createComponent(BlFileinputComponent);
        const component: BlFileinputComponent = fixture.debugElement.componentInstance;
        component.files = [new File(['content'], 'old.txt')];
        component.multiple = true;

        let angularNotified = false;
        component.registerOnChange((_) => angularNotified = true);

        spyOn(component.fileChanged, 'emit');

        component.fileChange({ target: { files: [] } });

        expect(component.fileChanged.emit).toHaveBeenCalledTimes(0);
        expect(angularNotified).toBeFalsy();
    }));
});
