# Angular 2 fileinput component
[![npm version](https://badge.fury.io/js/ng2-bl-fileinput.svg)](https://badge.fury.io/js/ng2-bl-fileinput)

A native fileinput component for angular 2.

See the [ng2-bl-fileinput] page for examples.

- [Getting started](#getting-started)
- [Input properties](#input-properties)
- [Output events](#output-events)

## Getting started

### Install

For npm users:
```
npm install --save ng2-bl-fileinput
```

For yarn users:
```
yarn add ng2-bl-fileinput
```

### Configuration

#### Angular cli

After installation, no additional configuration is needed. Import the
`BlFileinputModule` and define it as one of the imports of your application module:

```typescript
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BlFileinputModule} from 'ng2-bl-fileinput';

import {AppComponent} from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BlFileinputModule
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
```

## Input properties

| Name               | Type                          | Default               | Description                                                                                        |
| ------------------ | ----------------------------- | --------------------- | -------------------------------------------------------------------------------------------------- |
| accept             | `Array<string>`&#124;`string` | `null`                | List of allowed file types (MIME-Type or file extension) as array or comma separated as a string.  |
| buttonOnly         | `boolean`                     | `false`               | If set to true, only the fil selection button is shown.                                            |
| buttonText         | `string`                      | `choose file(s)`      | Only applies to single select. If set to true, a clickable clear selection cross is shown.         |
| icon               | `string`                      | `""`                  | CSS Class(es) for a Icon that is shown on the button.                                              |
| multiple           | `boolean`                     | `false`               | If set to true, the fileinput component is multi-select, otherwise single select.                  |
| placeholder        | `string`                      | `""`                  | Placeholder text that is shown if no options are selected.                                         |


## Output events

| Name          | Value                      | Description                                                              |
| ------------- | -------------------------- | ------------------------------------------------------------------------ |
| fileChanged   | `File`&#124;`Array<File>`  | The selected file(s).                                                    |

[ng2-bl-fileinput]: https://saithis.gitlab.io/ng2-bl-fileinput/
